<?php
// Start session
session_start();
$_SESSION['formTypeFeedback'] = true;

// Get the page name with all parameters
$url = basename($_SERVER['REQUEST_URI']);

if ($url == "feedback.php") {
    // No parameters
    if (!isset($_SESSION['success']) && !isset($_SESSION['errors'])) {
        header('Location: ../403.html');
    }
} else {
    // Use parse_url() function to parse the URL and return an array with the parameters
    $url_components = parse_url($url);

    // Use parse_str() function to parse the string passed via URL
    parse_str($url_components['query'], $params);
    $_SESSION['app_name'] = $params['app_name'];
    $_SESSION['app_version'] = $params['app_version'];
    $_SESSION['device'] = $params['device'];
    $_SESSION['os_version'] = $params['os_version'];
}
?>

<!DOCTYPE html>
<html lang="en-CA">
    <head>
        <meta charset="UTF-8">
        <!-- Block search indexing with meta tags -->
        <meta name="robots" content="noindex">
        <meta name="googlebot" content="noindex">
        <!-- Fix for intranet -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Favicons - https://realfavicongenerator.net -->
        <link rel="apple-touch-icon" sizes="180x180" href="../favicons/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="../favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="../favicons/favicon-16x16.png">
        <link rel="manifest" href="../favicons/site.webmanifest">
        <link rel="mask-icon" href="../favicons/safari-pinned-tab.svg" color="#525252">
        <link rel="shortcut icon" href="../favicons/favicon.ico">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-config" content="../favicons/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">
        <!-- CSS -->
        <link rel="stylesheet" href="../css/style.min.css">
        <!-- JS -->
        <script async src='https://www.google.com/recaptcha/api.js'></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script>
            // If CDN not avaible, load hosted jQuery
            if (typeof jQuery == 'undefined') {document.write(unescape("%3Cscript src='../js/jquery-3.2.1.min.js' type='text/javascript'%3E%3C/script%3E"))}

            // Google reCaptcha
            function captchaSubmit(token) {document.getElementById("contactForm").submit()}
        </script>
        <script src="../js/script.min.js"></script>
        <!-- Stuff -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes, viewport-fit=cover">
        <title><?php echo($_SESSION['app_name']) ?> - Feedback</title>
    </head>

    <body>
        <section>
            <div class="content">
                <div id="contact" class="feedback">
                    <div id="feedback">
                        <?php
                        $appFolderName = $_SESSION['app_name'];

                        if (strpos($appFolderName, " ") !== false) {
                          $appFolderName = str_replace(" ","-",$appFolderName);
                        }
                        ?>
                        <img src="<?php echo(strtolower($appFolderName)) ?>/app-icon.svg" alt="<?php echo $_SESSION['app_name'] ?>'s Mobile Application Icon">
                        <div>
                          <h2><?php echo $_SESSION['app_name'] ?></h2>
                          <h3>Feedback</h3>
                        </div>
                    </div>

                    <p class="info">Have you encountered an issue with the app? Is there a feature that you would like to have in an update? Feel free to submit your feedback using the form below.</p>
                    <form id="contactForm" method="post" action="../send_form.php">

                        <div id="formValidation">
                            <?php if(array_key_exists('errors',$_SESSION)): ?>
                            <? echo '<ul><li class="icon error">' . implode('</li><li class="icon error">', $_SESSION['errors']) . '</li></ul>'; ?>
                            <?php endif; ?>

                            <?php if(array_key_exists('success',$_SESSION)): ?>
                            <p class="icon success">Your feedback has been sent successfully</p>
                            <?php endif; ?>
                        </div>

                        <input type="text" name="name" id="name" required placeholder="First and last name" value="<?php echo isset($_SESSION['inputs']['name'])? $_SESSION['inputs']['name'] : ''; ?>">
                        <input type="email" name="mail" id="mail" required placeholder="Email address" value="<?php echo isset($_SESSION['inputs']['mail'])? $_SESSION['inputs']['mail'] : ''; ?>">
                        <br>
                        <div id="feedbackInfo">
                            <input type="text" name="device" id="device" required placeholder="Device Model" value="<?php echo isset($_SESSION['inputs']['device'])? $_SESSION['inputs']['device'] : $_SESSION['device']; ?>">
                            <input type="text" name="os_version" id="os_version" required placeholder="OS Version" value="<?php echo isset($_SESSION['inputs']['os_version'])? $_SESSION['inputs']['os_version'] : $_SESSION['os_version']; ?>">
                            <input type="text" name="app_version" id="app_version" required placeholder="Application Version" value="<?php echo isset($_SESSION['inputs']['app_version'])? $_SESSION['inputs']['app_version'] : $_SESSION['app_version']; ?>">
                        </div>
                        <br>
                        <textarea name="message" id="message" class="message" required placeholder="Message" rows="6"><?php echo isset($_SESSION['inputs']['message'])? $_SESSION['inputs']['message'] : ''; ?></textarea>
                        <br>
                        <label id="checkboxSendCopyByEmail"><input type="checkbox" name="sendCopyByEmail" id="sendCopyByEmail" value="yes" checked> I would like to receive a copy by Email</label>
                        <br>
                        <button id="send" class="g-recaptcha" data-sitekey="6LfX-rgUAAAAAAPUO_gKBX5PrBYNGZSnYUuNoyAp" data-callback="captchaSubmit" data-badge="inline">Send message</button>
                    </form>
                </div>
            </div>
        </section>
    </body>
</html>
<?php
// Reset last values
unset($_SESSION['inputs']);
unset($_SESSION['success']);
unset($_SESSION['errors']);
