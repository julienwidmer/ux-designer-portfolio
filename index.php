<?php
// Start session
session_start();
$_SESSION['formTypeFeedback'] = false;
?>

<!DOCTYPE html>
<html lang="en-CA" prefix="og: http://ogp.me/ns#">
    <head>
        <meta charset="UTF-8">
        <!-- Fix for intranet -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Favicons - https://realfavicongenerator.net -->
        <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
        <link rel="manifest" href="/favicons/site.webmanifest">
        <link rel="mask-icon" href="/favicons/safari-pinned-tab.svg" color="#525252">
        <link rel="shortcut icon" href="/favicons/favicon.ico">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-config" content="/favicons/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">
        <!-- OG - https://ogp.me -->
        <meta property="og:image" content="/favicons/og-image.png">
        <meta property="og:title" content="Julien Widmer - UX Designer">
        <meta property="og:description" content="Julien Widmer. A Swiss designer paying close attention to details creating not only the most enjoyable user experience but also the most intuitive one.">
        <meta property="og:url" content="https://www.julienwidmer.ca/">
        <meta property="og:locale" content="en_CA">
        <meta property="og:site_name" content="Julien Widmer">
        <meta property="og:type" content="website">
        <!-- CSS -->
        <link rel="stylesheet" href="css/style.min.css?v=1.1.0">
        <!-- JS -->
        <script async src='https://www.google.com/recaptcha/api.js'></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script>
            // If CDN not avaible, load hosted jQuery
            if (typeof jQuery == 'undefined') {document.write(unescape("%3Cscript src='js/jquery-3.2.1.min.js' type='text/javascript'%3E%3C/script%3E"))}

            // Google reCaptcha
            function captchaSubmit(token) {document.getElementById("contactForm").submit()}
        </script>
        <script src="js/script.min.js"></script>
        <!-- Stuff -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes, viewport-fit=cover">
        <title>Julien Widmer - UX Designer</title>
        <meta name="description" content="Julien Widmer. A Swiss designer paying close attention to details creating not only the most enjoyable user experience but also the most intuitive one."/>
    </head>

    <body>
        <header id="header">
            <div id="menu">
                <div class="content">
                    <a href="#header" id="logo"></a>

                    <p class="icon vertical mobileMenu">Menu</p>

                    <ul>
                        <li><a href="#projects" class="current" id="projectsLink">Projects</a></li>
                        <li><a href="#about" id="aboutLink">About</a></li>
                        <li><a href="#contact" id="contactLink">Contact</a></li>
                    </ul>
                </div>
            </div>

            <div id="intro" class="content">
                <h1>Julien Widmer - UX Designer</h1>
                <h1 class="bold">An international designer crafting pixel perfect designs!</h1>

                <img id="hero" src="img/hero-w600@2x.png" alt="A black see-through sealed and vacuumed plastic bag contains a smartphone. Printed on, the logo of Julien Widmer as well as the claim 'Let's give life to your crazy idea.' is visible.">
            </div>
        </header>

        <section>
            <div class="content">
                <div id="projects">
                    <h2>My latest projects</h2>
                    <div class="card">
                        <div class="info">
                            <div class="title">
                                <img src="img/logo/starbucks.svg" alt="Logo of Starbucks, a coffee company and coffeehouse chain.">
                                <h4>Starbucks</h4>
                            </div>
                            <h3>Implementation of a new feature to increase sales.</h3>
                            <p>In collaboration with another UX Designer, we have taken a closer look at the Starbucks Mobile App as of August 2019 in Canada and tried to implement a new feature to increase sales. Based on our researches, surveys and contextual analysis, we decided to integrate a new feature that would enable users to create grouped orders with coworkers or friends, to schedule orders in advance as well as to automate one (let say every weekday at 8:30 AM).</p>
                            <ul>
                                <li><a class="icon hover read" href="https://medium.com/@julienwidmer/the-feature-that-starbucks-could-implement-in-their-mobile-app-dae0dfb4cc96" title="Read the full Case Study of Starbucks on Medium.">Read Case Study</a></li>
                                <li><a class="icon hover vertical try" href="https://share.protopie.io/2kMngquQRyP" title="Try the final prototype of Starbucks on ProtoPie.">Try Prototype</a></li>
                            </ul>
                        </div>
                        <div class="visual">
                            <img src="img/projects/starbucks-Illustration-w475@2x.png" alt="Two smartphones standing up are displaying a new feature inside the Starbucks Mobile application.">
                        </div>
                    </div>

                    <div class="card">
                        <div class="info">
                            <div class="title">
                                <img class="hideInDarkMode" src="img/logo/herschel.svg" alt="Logo of Herschel, a manufacturer of backpacks and accessories.">
                                <img class="displayInDarkMode" src="img/logo/herschel-darkmode.svg" alt="Logo of Herschel, a manufacturer of backpacks and accessories.">
                                <h4>Herschel</h4>
                            </div>
                            <h3>Improvement of the user flow and user interface.</h3>
                            <p>In this project, 60% of the people who had been interviewed ended up frustrated and gave up their purchases on Herschel's website. By analyzing their behaviour, the current sitemap, doing card sorting and testing different prototypes with users to validate my hypothesis, I was able to simplify the user shopping experience and reduce the amount of click of 43% (from 7 to 4 clicks) for the same process only by doing navigation changes!</p>
                            <ul>
                                <li><a class="icon hover read" href="https://medium.com/@julienwidmer/how-to-improve-the-shopping-experience-of-herschel-online-b12a6946ee56">Read Case Study</a></li>
                                <li><a class="icon hover vertical try" href="https://invis.io/86TGYQ6YP4S" title="Try the final prototype of Herschel on InVision.">Try Prototype</a></li>
                            </ul>
                        </div>
                        <div class="visual">
                            <img src="img/projects/herschel-Illustration-w475@2x.png" alt="An open computer shows the redesigned interface of the website.">
                        </div>
                    </div>
                </div>

                <div id="clients">
                    <h2>Some of my clients</h2>
                    <img class="hideInDarkMode" src="img/logo/set24.svg" alt="Logo of set24, a SME.">
                    <img class="hideInDarkMode" src="img/logo/swisscom.svg" alt="Logo of Swisscom, one of the leading telecom operator and biggest company in Switzerland.">
                    <img class="hideInDarkMode" src="img/logo/wingo.svg" alt="Logo of Wingo, a telecom operator company.">
                    <img class="displayInDarkMode" src="img/logo/set24-darkmode.svg" alt="Logo of set24, a SME.">
                    <img class="displayInDarkMode" src="img/logo/swisscom-darkmode.svg" alt="Logo of Swisscom, one of the leading telecom operator and biggest company in Switzerland.">
                    <img class="displayInDarkMode" src="img/logo/wingo-darkmode.svg" alt="Logo of Wingo, a telecom operator company.">
                </div>

                <div id="about">
                    <h2>About me</h2>
                    <div class="info">
                        <div class="visual">
                            <img src="img/julienwidmer.jpg" alt="Picture of Julien Widmer in portrait mode. He is wearing a black t-shirt, glasses and has short brown hair.">
                            <ul>
                                <li><a class="icon hover linkedin" href="https://www.linkedin.com/in/julien-widmer" aria-label="Connect with me on LinkedIn.">Julien Widmer</a></li>
                            </ul>
                        </div>

                        <div class="text">
                            <p>I am a UX designer paying close attention to details creating not only the most enjoyable user experience but also the most intuitive one. Because I think that any user interface should not only look good but also be intuitive and logical, I take care of researching, analyzing and validating any design with users.</p>

                            <p>Passionate about IT and motivated to code from a young age, I can build websites and mobile applications from scratch using the following technologies: <span class="bold">HTML, CSS, Bootstrap, JavaScript, jQuery, Vue.js, PHP, MySQL</span> and <span class="bold">Swift.</span></p>

                            <p>Designer the day and developer at night, I am aware of the latest trends and products by reading the news daily and watching inspiring TED talks. Let's get in touch and give life to your project!</p>
                        </div>
                    </div>
                </div>

                <div id="contact">
                    <h2>Contact</h2>

                    <p class="info">With more than <span id="yearsOfExperience">5</span> years of experience in the industry and my ability to communicate in French, German and English, I am always looking for new challenges so feel free to send me a message or connect on <a href="https://www.linkedin.com/in/julien-widmer">LinkedIn</a>.</p>
                    <form id="contactForm" method="post" action="send_form.php">

                        <div id="formValidation">
                            <?php if(array_key_exists('errors',$_SESSION)): ?>
                            <? echo '<ul><li class="icon error">' . implode('</li><li class="icon error">', $_SESSION['errors']) . '</li></ul>'; ?>
                            <?php endif; ?>

                            <?php if(array_key_exists('success',$_SESSION)): ?>
                            <p class="icon success">Your message has been sent successfully</p>
                            <?php endif; ?>
                        </div>

                        <input type="text" name="name" id="name" required placeholder="First and last name" value="<?php echo isset($_SESSION['inputs']['name'])? $_SESSION['inputs']['name'] : ''; ?>">
                        <input type="email" name="mail" id="mail" required placeholder="Email address" value="<?php echo isset($_SESSION['inputs']['mail'])? $_SESSION['inputs']['mail'] : ''; ?>">
                        <br>
                        <textarea name="message" id="message" class="message" required placeholder="Message" rows="6"><?php echo isset($_SESSION['inputs']['message'])? $_SESSION['inputs']['message'] : ''; ?></textarea>
                        <br>
                        <label id="checkboxSendCopyByEmail"><input type="checkbox" name="sendCopyByEmail" id="sendCopyByEmail" value="yes" checked> I would like to receive a copy by Email</label>
                        <br>
                        <button id="send" class="g-recaptcha" data-sitekey="6LfX-rgUAAAAAAPUO_gKBX5PrBYNGZSnYUuNoyAp" data-callback="captchaSubmit" data-badge="inline">Send message</button>
                    </form>
                </div>
            </div>
        </section>

        <footer>
            <div class="content">
                <div id="footer">
                    <img src="img/logo/julienwidmer-primary-monochrome-white.svg" alt="Logo of Julien Widmer.">

                    <div id="links">
                        <ul id="nav">
                            <li>Discover</li>
                            <li><a href="#projects">Projects</a></li>
                            <li><a href="#about">About</a></li>
                            <li><a href="#contact">Contact</a></li>
                        </ul>

                        <ul id="social">
                            <li>Social</li>
                            <li><a class="icon hover linkedin" href="https://www.linkedin.com/in/julien-widmer" aria-label="Connect with me on LinkedIn.">Julien Widmer</a></li>
                            <li><a class="icon hover gitlab" href="http://gitlab.com/user/@julienwidmer" aria-label="Follow me on GitLab.">@julienwidmer</a></li>
                            <li><a class="icon hover medium" href="http://medium.com/@julienwidmer" aria-label="Follow me on Medium.">@julienwidmer</a></li>
                        </ul>
                    </div>
                </div>
                <p>Last updated May 4, 2020</p>
            </div>
        </footer>
    </body>
</html>
<?php
// Reset last values
unset($_SESSION['inputs']);
unset($_SESSION['success']);
unset($_SESSION['errors']);
