# UX Designer Portfolio
<img src="img/logo/julienwidmer-w-shape-monochrome-black.svg" align="right" height="50px">
A clean and mobile responsive UX Designer's website.


#  Features
* Made for Responsive Design
* Clean code with comments for an easy maintenance
* jQuery and CSS Animations
* Visuals and files have been optimized to load quicker
* Form validator and reCAPTCHA v2
* HTML and CSS validated by the W3C Validation Service without errors
* Automatically detect and switch to dark mode


# Live Preview
I am currently using this website for my portfolio who can be accessed at [julienwidmer.ca](https://www.julienwidmer.ca).


# Author
Hi! I’m [Julien Widmer](https://www.julienwidmer.ca) - Feel free to send me an [Email](mailto:hello@julienwidmer.ca) or to follow me on [Twitter](http://twitter.com/Qasph).