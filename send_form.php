<?php
// Start session
session_start();
// Verify info
$errors = array();

$formTypeFeedback = $_SESSION['formTypeFeedback'];

if(!array_key_exists('name', $_POST) || $_POST['name'] == '') {
    $errors ['name'] = "Please provide your first and last name";
}

if(!array_key_exists('mail', $_POST) || $_POST['mail'] == '' || !filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL)) {
    $errors ['mail'] = "Please provide a valid Email address";
}

if ($formTypeFeedback) {
    if(!array_key_exists('device', $_POST) || $_POST['device'] == '') {
        $errors ['device'] = "Please provide the model of your device";
    }

    if(!array_key_exists('os_version', $_POST) || $_POST['os_version'] == '') {
        $errors ['os_version'] = "Please provide the OS version your device is running on";
    }
    
    if(!array_key_exists('app_version', $_POST) || $_POST['app_version'] == '') {
        $errors ['app_version'] = "Please provide the application version";
    }
}

if(!array_key_exists('message', $_POST) || $_POST['message'] == '') {
    $errors ['message'] = "Please provide a message";
}


// If there are errors, save them and go to index.php otherwise verify reCAPTCHA and send Email
if(!empty($errors)) {
    $_SESSION['errors'] = $errors;
    $_SESSION['inputs'] = $_POST;
    
    if ($formTypeFeedback) {
        header('Location: app/feedback.php#contact');
    } else {
        header('Location: index.php#contact');
    }
} else {
    // reCAPTCHA info
    $secret = 'MY RECAPTCHA SECRET KEY';
    $remoteip = $_SERVER['REMOTE_ADDR'];
    $url = 'https://www.google.com/recaptcha/api/siteverify';

    // Form info
    $response = $_POST['g-recaptcha-response'];

    // Curl Request
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, array(
        'secret' => $secret,
        'response' => $response,
        'remoteip' => $remoteip
    ));
    $curlData = curl_exec($curl);
    curl_close($curl);

    // Get reCAPTCHA answer in JSON and convert it in array
    $recaptcha = json_decode($curlData, true);

    if ($formTypeFeedback) {
        $element = 'feedback';
    } else {
        $element = 'message';
    }
    
    $message = '';
    
    if ($recaptcha["success"]) {
        $_SESSION['success'] = 1;
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= 'FROM: '. htmlspecialchars($_POST['name']) .' <'. htmlspecialchars($_POST['mail']) .'>';
        $to = 'MY EMAIL ADDRESS';
        $subject = 'New ' . $element . ' - julienwidmer.ca';
        
        
        // Message to send
        $message_content = '
        <table>
            <tr>
                <td>
                    <b>First and last name:</b>
                </td>
            </tr>
            
            <tr>
                <td>'. htmlspecialchars($_POST['name']) .'</td>
            </tr>
            
            <tr>
                <td>
                    <b>Email address:</b>
                </td>
            </tr>
            
            <tr>
                <td>'. htmlspecialchars($_POST['mail']) .'</td>
            </tr>
        ';
        
        if ($formTypeFeedback) {
            $message_content .= '
            <tr>
                <td>
                    <b>Application Name:</b>
                </td>
            </tr>
            
            <tr>
                <td>'. htmlspecialchars($_SESSION['app_name']) .'</td>
            </tr>
            
            <tr>
                <td>
                    <b>Application Version:</b>
                </td>
            </tr>
            
            <tr>
                <td>'. htmlspecialchars($_POST['app_version']) .'</td>
            </tr>
            
            <tr>
                <td>
                    <b>Device:</b>
                </td>
            </tr>
            
            <tr>
                <td>'. htmlspecialchars($_POST['device']) .'</td>
            </tr>
            
            <tr>
                <td>
                    <b>OS Version:</b>
                </td>
            </tr>
            
            <tr>
                <td>'. htmlspecialchars($_POST['os_version']) .'</td>
            </tr>
            ';
        }
        
        $message_content .= '
            <tr style="height: 10px"><td></td></tr>
            
            <tr>
                <td>
                    <b>' . ucwords($element) . ':</b>
                </td>
            </tr>

            <tr>
                <td>'. nl2br(htmlspecialchars($_POST['message'])) .'</td>
            </tr>
        </table>
        ';
        
        $message = $message_content;
        
        mail($to, $subject, $message_content, $headers);

        
        // Send copy to sender
        if (isset($_POST['sendCopyByEmail'])) {
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            $headers .= 'FROM: "Julien Widmer" <MY EMAIL ADDRESS>';
            $to = htmlspecialchars($_POST['mail']);
            $subject = 'Copy of your ' . $element . ' - julienwidmer.ca';
            
            // Message to send
            $message_content = '
            <table>
                <tr>
                    <td>
                        Dear '. htmlspecialchars($_POST['name']) .',<br>Thank you for your ' . $element . '.
                    </td>
                </tr>

                <tr style="height: 30px"><td></td></tr>

                <tr>
                    <td>---</td>
                </tr>

                <tr>
                    <td>Please note that this Email is not a reply and is only a copy provided for your record.</td>
                </tr>

                <tr>
                    <td>---</td>
                </tr>

                <tr style="height: 10px"><td></td></tr>
            </table>
            '. $message .'
            ';
            
            mail($to, $subject, $message_content, $headers);
        } // END - Copy to sender

        if ($formTypeFeedback) {
            header('Location: app/feedback.php#contact'); // Go back to index.php
        } else {
            header('Location: index.php#contact'); // Go back to index.php
        }
    } else {
        echo 'Oops... Something wrong happened, please try again later :(';
    }
}