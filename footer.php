<?php
$domain = "https://julienwidmer.ca"
?>
<div id="footer">
    <img src="<?php echo($domain) ?>/img/logo/julienwidmer-primary-monochrome-white.svg" alt="Logo of Julien Widmer.">

    <div id="links">
        <ul id="nav">
            <li>Discover</li>
            <li><a href="<?php echo($domain) ?>/#projects">Projects</a></li>
            <li><a href="<?php echo($domain) ?>/#about">About</a></li>
            <li><a href="<?php echo($domain) ?>/#contact">Contact</a></li>
            <li><a href="<?php echo($domain) ?>/download/Widmer_Julien_UX_Designer_CV.pdf">Download résumé</a></li>
        </ul>

        <ul id="social">
            <li>Social</li>
            <li><a class="icon hover linkedin" href="https://www.linkedin.com/in/julien-widmer" aria-label="Connect with me on LinkedIn.">Julien Widmer</a></li>
            <li><a class="icon hover gitlab" href="http://gitlab.com/user/@julienwidmer" aria-label="Follow me on GitLab.">@julienwidmer</a></li>
            <li><a class="icon hover medium" href="http://medium.com/@julienwidmer" aria-label="Follow me on Medium.">@julienwidmer</a></li>
        </ul>
    </div>
</div>