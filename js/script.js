$(document).ready(function () {
    // Update years in contact section
    var d = new Date();
    var currentYear = d.getFullYear();

    document.getElementById("yearsOfExperience").innerHTML = currentYear - 2014;



    // Add smooth scrolling to all links
    $("a").on('click', function(event) {

        // Make sure this.hash has a value before overriding default behavior
        var linkHasValue = this.hash !== "";
        var linkIsTheLogo = this.hash == "#header";
        var userIsNotOnTop = $(window).scrollTop() !== 0;

        if ((linkHasValue && !linkIsTheLogo) || (linkIsTheLogo && userIsNotOnTop)) {
            // If the link has a value and it is not the logo or if it is the logo but the user had scroll
            if (linkIsTheLogo && $("#menu").hasClass("open")) {
                // If the link is the logo and the mobile menu is open then close it
                $("#menu").removeClass("open");
                $("#menu .mobileMenu").text("Menu");
            }

            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Smooth scroll to anchor in 800 milliseconds
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 800, function(){
                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        } // End if
    });



    // Check scroll distance for highlighting current section
    $(window).scroll(function() {
        var userScrollDistance = $(window).scrollTop();
        var aboutScrollDistance = ($("#about").offset().top - 60); // Menu height is 60px by default so substract it
        var contactScrollDistance = ($("#contact").offset().top - 60); // Menu height is 60px by default so substract it

        $('#menu ul li a').removeClass('current');

        switch(true) {
            case (userScrollDistance >= aboutScrollDistance && userScrollDistance < contactScrollDistance):
                // User has scrolled to the About section
                $('#aboutLink').addClass('current');
                break;
            case (userScrollDistance >= contactScrollDistance):
                // User has scrolled to the Contact section
                $('#contactLink').addClass("current");
                break;
            default:
                // Projects is highlighted by default if user has not scrolled enough
                $('#projectsLink').addClass('current');
        }
    });



    // Mobile Menu
    $( "#menu .mobileMenu, #menu ul li a" ).click(function() {
        if ($("#menu").hasClass("open")) {
            // Mobile Menu has to be closed
            $("#menu").removeClass("open");
            $("#menu .mobileMenu").text("Menu");
        } else {
            // Mobile Menu has to be open
            $("#menu").addClass("open");
            $("#menu .mobileMenu").text("Close");
        }
    });
}) // END - $(document).ready(function ()